PhoneList App - Frontend Challenge Etereo.

@author: Alberto Martin


This app displays a list of 3 models of phones with an image attached to it.

  ## Exercise 1: Write a simple REST API in whatever language you're most comfortable (NodeJS, Rails, Java...) that...

    - has 1 endpoint /phones
    - returns a couple of mobile phones like iPhone 7, Samsung Galaxy S7 etc. with
    some simple attributes like product image (just take some off of google images), title,
    description, color, price etc.
    - The data can all be mocked, no need for a persistence layer
  
  ## Answer to Exercise 1:

    You can find all the relevant information regarding this first part in the files: /phoneApp/server.js and 
    /phoneApp/phones.json.

    Server.js is a basic and simple nodejs server with a mocked /phones endpoint. In order to keep server.js 
    file clean and tidy, it reads the phones payload from an external file (phones.json), and after 
    a small, and intentional delay (with the intention that Frontend shows a spinning component), 
    it returns that payload to the frontend.


  ## Exercise 2: Write a React app that displays the phones from the API:
    - Use redux for state management
    - Axios(or similar library) for fetching data from the API
    - Create a `PhoneListContainer` component that shows the phones
    - Create a `PhoneDetailComponent` that shows a few more details about the phone the user selects it
    - Display a spinner or placeholder component while the API request is ongoing
    - Make it look decent. No need for super sophisticated design, but at a minimum, make it somewhat responsive 
    so that it doesn’t look terrible on a mobile phone.

  ## Answer to Exercise 2:

    First thing I did when I decided to do this exercise is to decide the libraries I was going to use. 
    Of course, it's mandatory to use npm packages like redux and react-redux, but I also decided to use bootstrap, 
    as it provides nice and easy classes to keep the content in place, and make it responsive on mobile phones.

    I also think it's interesting to include bootstrap so I can show that I have knowledge about it, and the features 
    and components it provides. As an example, I considered the "PhoneDetailComponent" component, 
    a Bootstrap accordion component.

    Furthermore, I was told in the interview that it'd be interesting to use a React library called "styled-components". 
    So I included it, and used to create some DOM elements.

    After that I'd also need express.js, axios and fontawesome (to display a spinning wheel).

    For redux purposes, I am using redux, react-redux and redux-thunk libraries. I also created:

      1- Reducers --> /phoneApp/src/reducers/

      2- Store --> /phoneApp/src/store.js

      3- Actions --> /phoneApp/src/actions/phoneActions.js


  ### Components:
    darkNavBar: phoneApp/src/components/darkNavBar.js
      A navbar component that serves as a header that we can use in the future if we need to add additional 
      routes to our App
    
    PhoneListContainer: phoneApp/src/components/phoneListContainer.js
      It consists on a table that displays information about every phone and has embedded a phoneDetailComponent component.
    
    phoneDetailComponent: phoneApp/src/components/phoneDetailComponent.js
      This component is a bootstrap accordion that collapses when the user clicks "Display Picture".
    
    loadingSpinner: phoneApp/src/components/loadingSpinner.js
      nodeJS server has on -GET: /phones an intentional delay of 2000 ms; you'll be able to see that spinner whilst we wait for 
      those 2 seconds for the request to be fulfilled.

  ### How to run
    1- npm install
    2- npm start - Open a command line to make react run.
    3- node server - Open another command line to make nodejs server run.
    4- Navigate to localhost:3000 and you'll see the application.

  ### Improvements:
    1- Some further work could have been done was to build, and compress *.css and *.js files.
    2- Test structure is built, but no test have been written yet. It's a WIP as I need to play a bit more with JEST. (I come from an Ember/Karma Background)
    3- Deploy


