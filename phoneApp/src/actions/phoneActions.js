import { FETCH_PHONES } from "./types";
import axios from "axios";

export const fetchPhones = () => dispatch =>{
	return axios.get(
		"/phones"
	)
	.then(phones=>{
		return dispatch({
			type:FETCH_PHONES,
			payload:phones.data,
			isLoading:false
		})
	})
}
