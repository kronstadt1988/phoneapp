import {FETCH_PHONES} from '../actions/types';

const initialState = {
	items:[], 
	item:{},
	isLoading:true
}

export default function (state = initialState, action){
	switch(action.type){
		case FETCH_PHONES:
			return{
				...state,
				items: action.payload,
				isLoading: action.isLoading
			}
			
		default:
			return state;
	}
}