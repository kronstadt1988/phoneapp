import React, { Component } from "react";
import { fetchPhones } from "../actions/phoneActions";
import { connect } from "react-redux";
import PhoneDetailComponent from "./phoneDetailComponent";
import LoadingSpinner from './loadingSpinner'


class PhoneListContainer extends Component {
	async componentWillMount() {
		await this.props.fetchPhones();
	}

	render() {
		if (this.props.isLoading) {
			return <LoadingSpinner/>;
		}
		const phoneItems = this.props.phones.phones.map(phone => (
			<tbody key={phone.id}>
				<tr>
					<th>{phone.id}</th>
					<td>{phone.model}</td>
					<td>{phone.price} €</td>
					<td>{phone.description}</td>
					<td>{phone.color}</td>
					<td>
						<a
							data-toggle="collapse"
							href={`#collapseExample${phone.id}`}
							role="button"
							aria-expanded="false"
							aria-controls={`#collapseExample${phone.id}`}
						>
							Display Picture
						</a>
					</td>
				</tr>
				<tr className="collapse" id={`collapseExample${phone.id}`}>
					<td colSpan="6">
						<div>
							<div>
								<PhoneDetailComponent phoneUrl={phone.url}/>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		));

		return (
			<div className="container-fluid">
				<div className="row">
					<table className="table table-hover table-dark">
						<thead>
							<tr>
								<th scope="col">Id</th>
								<th scope="col">Model</th>
								<th scope="col">Price</th>
								<th scope="col">Description</th>
								<th scope="col">Color</th>
							</tr>
						</thead>
						{phoneItems}
					</table>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	phones: state.phones.items,
	isLoading: state.phones.isLoading
});

export default connect(
	mapStateToProps,
	{ fetchPhones }
)(PhoneListContainer);
