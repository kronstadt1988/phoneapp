import React, { Component } from "react";
import styled from 'styled-components';

const BigSpinner = styled.i`
  font-size:48px !important;
  margin-top:20px;
`;

class LoadingSpinner extends Component {

	render() {
		return (
			<BigSpinner className="fa fa-gear fa-spin"></BigSpinner>
		);
	}
}

export default LoadingSpinner
