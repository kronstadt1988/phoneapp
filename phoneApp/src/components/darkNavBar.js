import React, { Component } from "react";
import styled from 'styled-components';

const StyledLinkForNavBar = styled.a`
  font-size:22px;
  padding:10px;
  color:white;
  
`;

const StyledLinkUnderlineForNavBar = styled(StyledLinkForNavBar)`
  letter-spacing: 3px;
  text-decoration: underline grey;
`;

const NavBar = styled.nav`
  justify-content:baseline;
  position:relative;
  display:flex;
  flex-wrap:wrap;
  align-items:center;
  justify-content:baseline;
`;

class DarkNavBar extends Component {

  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <NavBar className="container-fluid navbar-dark bg-dark">
            <StyledLinkForNavBar href='#'>
              Phonelist App
            </StyledLinkForNavBar>
            <StyledLinkUnderlineForNavBar href='#'>
              Inherited and Spaced Link
            </StyledLinkUnderlineForNavBar>
          </NavBar>
        </div>
      </div>
    );
  }
}

export default DarkNavBar;
