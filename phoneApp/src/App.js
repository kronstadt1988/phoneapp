import React, { Component } from "react";
import "./App.css";
import DarkNavBar from "./components/darkNavBar";
import PhoneListContainer from "./components/phoneListContainer";
import {Provider} from 'react-redux';
import store from './store'

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <div className="App">
          
          <DarkNavBar />
          <PhoneListContainer/>
        </div>
      </Provider>
    );
  }
}

export default App;
