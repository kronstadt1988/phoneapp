const express = require("express");
const path = require("path");
const app = express();
const port = 5000;
const fs = require("fs");

app.use(express.static(path.join(__dirname, "public")));

app.get("/", (req, res) => {
	res.sendFile(path.join(__dirname + "/public/index.html"));
});

app.get("/phones", (req, res) => {
	
	let phones = fs.readFileSync("phones.json");
	let parsedPhones = JSON.parse(phones);
	
	setTimeout(()=>{
		res.send(parsedPhones)
	}, 2000)
	
});

app.listen(port, () => console.log(`Listening on port ${port}`));